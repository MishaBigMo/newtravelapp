//
//  TravelCell.swift
//  Travel App
//
//  Created by Михаил on 26/04/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class TravelCell: UITableViewCell {
    
    @IBOutlet weak var travelNameLabel: UILabel!
    @IBOutlet weak var travelSubtitleLabel: UILabel!
    
    @IBOutlet weak var rankTravel: UIStackView!
    
    @IBOutlet weak var oneStar: UIImageView!
    @IBOutlet weak var twoStar: UIImageView!
    @IBOutlet weak var threeStar: UIImageView!
    @IBOutlet weak var fourStar: UIImageView!
    @IBOutlet weak var fiveStar: UIImageView!
}
