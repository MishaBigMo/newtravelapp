//
//  UIView+CoreKit.swift
//  Travel App
//
//  Created by Михаил on 12/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import UIKit
import Foundation

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get { // срабатывает когда хочу забрать значение
            return layer.cornerRadius
        }
        set { // срабатывает когда хочу всунуть новое, если нет, то неьхя изменить
           layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
}
