//
//  RealmNotificationExample.swift
//  Travel App
//
//  Created by Михаил on 05/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import RealmSwift
import UIKit

class RealmNotificationExample: UIViewController {
    
    var travel: RealmTravel?
    var notificationToken: NotificationToken? = nil
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBAction func createTravel(_ sender: Any) {
        travel = RealmTravel()
        travel?.id = "XXXXXX"
        travel?.name = "safasfas"
        DataBaseManager.instance.save(travel)
       // DataBaseManager.instance.saveTravelToDataBase(travel: travel!)
        notificationToken = travel?.observe({ (change) in
            switch change {
            case .change(let properties):
                for property in properties {
                    if property.name == "name" {
                        self.textLabel.text = property.newValue as? String
                    }
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("The object was deleted.")
            }
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
