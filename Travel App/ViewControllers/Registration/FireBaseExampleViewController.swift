//
//  FireBaseExampleViewController.swift
//  Travel App
//
//  Created by Михаил on 21/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Firebase
import UIKit

class FireBaseExampleViewController: UIViewController {
    // MARK: - Properties
    
    // MARK: - Outlets
    
    // MARK: - Actions
    
    @IBAction func createAccount(_ sender: Any) {
        Auth.auth().createUser(withEmail: "2@test.com", password: "123123123") { (result, error) in
            if error == nil {
                print(result?.user.email)
            } else {
                print("error не смогли зарегать")
            }
        }
    }
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }


}
