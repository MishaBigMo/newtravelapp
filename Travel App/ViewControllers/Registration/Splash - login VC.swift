//
//  Splash - login VC.swift
//  Travel App
//
//  Created by Михаил on 15/04/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Firebase
import UIKit

class Splash___login_VC: UIViewController { // welcomeVC
    
    // MARK: - Outlets
    
    @IBOutlet weak var moreWaysToLoginButton: UIButton!
    
    
    // MARK: - Actions
    
    @IBAction func reservButtonDidTapped(_ sender: Any) {
        let controller = UIViewController.getFromStoryboard(withId: "TravelListVC")!
        navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func loginWithEmailTapped(_ sender: Any) {
        let loginController = UIViewController.getFromStoryboard(withId: "login")!
        navigationController?.pushViewController(loginController, animated: true)
    }
    
    @IBAction func registrationTapped(_ sender: Any) {
        // new accaunt
        let loginController = UIViewController.getFromStoryboard(withId: "createAccauntVC")
        navigationController?.pushViewController(loginController!, animated: true)
    }
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.fetch { (status, error) in
            remoteConfig.activateFetched()
            if let buttonTitle = remoteConfig["buttonTitle"].stringValue {
                self.moreWaysToLoginButton.setTitle(buttonTitle, for: .normal)
            }
            let isButtonHidden = remoteConfig["buttonHidden"].boolValue
            self.moreWaysToLoginButton.isHidden = isButtonHidden
            
        }
    }
    
}

