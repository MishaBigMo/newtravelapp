//
//  EmailShowPasswordViewController.swift
//  Travel App
//
//  Created by Михаил on 15/04/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Firebase
import UIKit

class EmailShowPasswordViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var passwordView: UILabel!
    @IBOutlet weak var loginLabel: UITextField!
    @IBOutlet weak var viewMini: UIView!
    @IBOutlet weak var isntShow: UIButton!
    @IBOutlet weak var isShow: UIButton!
    @IBOutlet weak var passwordLabelOutlet: UITextField!
    
    // MARK: - Actions
    @IBAction func forgotDidTapped(_ sender: Any) {
        let controller = UIViewController.getFromStoryboard(withId: "forgotVC")!
        navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
 
        if let login = loginLabel.text, let password = passwordLabelOutlet.text {
            Auth.auth().signIn(withEmail: login, password: password) { (result, error) in
                if error == nil {
                    let user = result?.user
                    print(user?.email)
                    
                    let travelVC = UIViewController.getFromStoryboard(withId: "TravelListVC")
                    self.navigationController?.pushViewController(travelVC!, animated: true)
                } else {
                    print("Ошибка логина!")
                    self.viewMini.backgroundColor = .red
                    self.passwordView.textColor = .red
                }
            }
            
        }
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func secureButtonActionIsHidden(_ sender: Any) {
        passwordLabelOutlet.isSecureTextEntry = false
        isShow.isHidden = true
        isntShow.isHidden = false
    }
    
    @IBAction func secureButtonActionNotHidden(_ sender: Any) {
        passwordLabelOutlet.isSecureTextEntry = true
        isShow.isHidden = false
        isntShow.isHidden = true
    }
    
    @IBAction func testSecureButtonActionNotHidden(_ sender: UIButton) {
        sender.isSelected.toggle()
        passwordLabelOutlet.isSecureTextEntry.toggle()
    }
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
