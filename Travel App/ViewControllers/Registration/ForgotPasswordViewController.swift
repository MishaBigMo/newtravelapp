//
//  ForgotPasswordViewController.swift
//  Travel App
//
//  Created by Михаил on 16/04/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Firebase
import UIKit

class ForgotPasswordViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    //MARK: - Actions
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordDidTapped(_ sender: Any) {
        if let email = emailTextField.text {
            Auth.auth().sendPasswordReset(withEmail: email) { (error) in
                if error == nil {
                    print("новый пароль отпрвлен успешно")
                } else {
                    print("новый пароль не отправлен!")
                }
            }
        }
    }
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
