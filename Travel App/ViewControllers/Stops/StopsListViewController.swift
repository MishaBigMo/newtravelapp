//
//  StopsListViewController.swift
//  Travel App
//
//  Created by Михаил on 03/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit


class StopsListViewController: UIViewController {
    // MARK: - Properties
    var realmTravel: RealmTravel!
    var arrayStopsRank: [Int] = []
    var sumArrayStopsRank = 0
    var count = 0
    var delegate: TravelsListViewController?
    var a = 1

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var travelLabel: UILabel!
    
    // MARK: - Actions
    @IBAction func addCreateStopDidTapped(_ sender: Any) {
        let createStop = UIViewController.getFromStoryboard(withId: "CreateStop") as! CreateStopViewController
        createStop.delegate = self
        createStop.delegateStop = self
        createStop.stopDidCreateClosure = { stop in
            DataBaseManager.instance.addStop(stop, to: self.realmTravel)
            //DataBaseManager.instance.save(self.realmTravel)
            DataBaseManager.instance.saveTravelToDataBase(travel: self.realmTravel)
        }
        navigationController?.pushViewController(createStop, animated: true)
    }
    
    @IBAction func backDidTapped(_ sender: Any) {
            delegate?.tableView.reloadData()
            navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        travelLabel.text = realmTravel.name
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    
}


extension StopsListViewController: UITableViewDelegate, UITableViewDataSource, CreteStopViewControllerDelegate {
    
    func createStopControllerDidCreateStop(_ stop: RealmStop) {
        
        realmTravel.stops.append(stop)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DataBaseManager.instance.deleteStopFromDataBase(realmTravel.stops[indexPath.row])
            self.tableView.deleteRows(at:[indexPath],with: .fade)

            //self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmTravel.stops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopCell", for: indexPath) as! StopCell
        
        let stop = realmTravel.stops[indexPath.row]
        cell.titleLabel.text = stop.name
        cell.subTitleLabel.text = stop.details
        cell.moneyLabel.text = stop.money
        
        switch stop.transport {
        case "Jet":
            cell.transportImageView.image = UIImage(named: "jet")
        case "Car":
            cell.transportImageView.image = UIImage(named: "car")
        case "Train":
            cell.transportImageView.image = UIImage(named: "train")
        default:
            print("error switch transport \(stop.transport)")
        }

        switch stop.rank {
        case 1:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOff")
            cell.threeStar.image = UIImage(named: "StarOff")
            cell.fourStar.image = UIImage(named: "StarOff")
            cell.fiveStar.image = UIImage(named: "StarOff")
            arrayStopsRank.append(1)    
            
        case 2:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOn")
            cell.threeStar.image = UIImage(named: "StarOff")
            cell.fourStar.image = UIImage(named: "StarOff")
            cell.fiveStar.image = UIImage(named: "StarOff")
            arrayStopsRank.append(2)
            
        case 3:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOn")
            cell.threeStar.image = UIImage(named: "StarOn")
            cell.fourStar.image = UIImage(named: "StarOff")
            cell.fiveStar.image = UIImage(named: "StarOff")
            arrayStopsRank.append(3)
            
        case 4:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOn")
            cell.threeStar.image = UIImage(named: "StarOn")
            cell.fourStar.image = UIImage(named: "StarOn")
            cell.fiveStar.image = UIImage(named: "StarOff")
            arrayStopsRank.append(4)
            
        case 5:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOn")
            cell.threeStar.image = UIImage(named: "StarOn")
            cell.fourStar.image = UIImage(named: "StarOn")
            cell.fiveStar.image = UIImage(named: "StarOn")
            arrayStopsRank.append(5)
            
        default:
            print("ups...")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateStop") as! CreateStopViewController
        controller.stop = realmTravel.stops[indexPath.row]
        controller.delegateFromStopListViewController = self
        controller.stopDidCreateClosure = { stop in
            DataBaseManager.instance.saveTravelToDataBase(travel: self.realmTravel)
        }
        navigationController?.pushViewController(controller, animated: true)
    }
}

