//
//  AnimationViewController.swift
//  Travel App
//
//  Created by Михаил on 07/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {

    @IBOutlet weak var firstSquare: UIView!
    
    @IBOutlet weak var secondSquare: UIView!
    
    
    @IBAction func thirdDidTapped(_ sender: Any) {
        let scale = CABasicAnimation(keyPath: "transform.scale")
        scale.duration = 0.9
        scale.fromValue = 1 // начальное значение
        scale.toValue = 1.5 // к какому значению
        scale.repeatCount = .infinity // бесконечно раз повторяется
        scale.autoreverses = true
        
        secondSquare.layer.add(scale, forKey: "asd")
    }
    @IBOutlet weak var secondSquareTopConstraint: NSLayoutConstraint!
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        secondSquareTopConstraint.constant = 300
        UIView.animate(withDuration: 0.5) {
            self.secondSquare.transform = CGAffineTransform(scaleX: 0.2, y: 0.5)
            self.secondSquare.frame.origin.y += 90
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func firstSquareDidTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.firstSquare.backgroundColor = .blue
            self.firstSquare.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.firstSquare.frame.origin.y += 90
            self.firstSquare.layer.cornerRadius = self.firstSquare.frame.size.width / 2
        }) { (success) in
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

}
