//
//  RealmNptificationExampleSecondViewController.swift
//  Travel App
//
//  Created by Михаил on 05/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class RealmNptificationExampleSecondViewController: UIViewController {
    var travel: RealmTravel?

    @IBOutlet weak var textField: UITextField!
    
    @IBAction func textFieldDidChanged(_ sender: Any) {
        DataBaseManager.instance.updateTravel(travel!, withName: textField.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       travel = DataBaseManager.instance.getTravelFromDataBase(withId: "XXXXXX")
        
    }
}


extension RealmNptificationExampleSecondViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
    }
}
