//
//  Todos.swift
//  Travel App
//
//  Created by Михаил on 30/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class Todos {
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var completed: Bool = false

}
