//
//  User.swift
//  Travel App
//
//  Created by Михаил on 17/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Foundation

class User: CustomStringConvertible {
    var id: Int = 0
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var phone: String = ""
    var website: String = ""
    var address: Address?
    var company: Company?
    var description: String {
        return """
        \n
        User:
        id - \(id)
        name - \(name)
        username - \(username)
        email - \(email)
        phone - \(phone)
        website - \(website)
        
        Address:
        street - \(address?.street)
        suite - \(address?.suite)
        city - \(address?.city)
        zipcode - \(address?.zipcode)
        
        Geo:
        latitude - \(address?.geo?.latitude)
        longitude - \(address?.geo?.longitude)
        
        Company:
        name - \(company?.name)
        catchPhrase - \(company?.catchPhrase)
        bs - \(company?.bs)
        \n
        """
    }
    func printAll() {
        print("""
            \n
            User:
            id - \(id)
            name - \(name)
            username - \(username)
            email - \(email)
            phone - \(phone)
            website - \(website)

            Address:
            street - \(address?.street)
            suite - \(address?.suite)
            city - \(address?.city)
            zipcode - \(address?.zipcode)

            Geo:
            latitude - \(address?.geo?.latitude)
            longitude - \(address?.geo?.longitude)

            Company:
            name - \(company?.name)
            catchPhrase - \(company?.catchPhrase)
            bs - \(company?.bs)
            \n
            """)
    }
}
