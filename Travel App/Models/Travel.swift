//
//  Travel.swift
//  Travel App
//
//  Created by Михаил on 07/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class Travel {
    var name: String = ""
    var desc: String = ""
    var stops: [Stop] = []
    
}
