//
//  User.swift
//  Travel App
//
//  Created by Михаил on 17/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation
class User : Decodable {
    
    var id: Int = 0
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var phone: String = ""
    var website: String = ""
    var address: Address?
    var company: Company?
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case username
        case email
        case phone
        case website
        case address
        case company
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try! values.decode(Int.self, forKey: .id)
        self.name = try! values.decode(String.self, forKey: .name)
        self.username = try! values.decode(String.self, forKey: .username)
        self.email = try! values.decode(String.self, forKey: .email)
        self.phone = try! values.decode(String.self, forKey: .phone)
        self.website = try! values.decode(String.self, forKey: .website)
        self.company = try! values.decode(Company.self, forKey: .company)
        self.address = try values.decode(Address.self, forKey: .address)
        
    }
    
}

extension User: CustomStringConvertible {
    var description : String {
        return """
        \n
        id: \(id )
        name: \(name )
        email: \(email)
        phone:\(phone )
        website: \(website)
        \n
        company:
        \(company?.name)
        \(company?.bs))
        \(company?.catchPhrase))
        \n
        address:
        \(address?.city)
        \(address?.street)
        \(address?.suite)
        \(address?.zipcode)
        Geo:
        \(address?.geo?.lat)
        \(address?.geo?.lng)
        \n
        """
    }
}
