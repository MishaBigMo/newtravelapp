//
//  Stop.swift
//  Travel App
//
//  Created by Михаил on 19/04/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class Stop {
    
    var money: String = ""
    var name: String = ""
    var rank: Int = 0
    var transport: String = ""
    var details: String = ""
    var rating: Int = 0
    func printAll() {
        print("""
            Название - \(name)
            Рейтинг - \(rank)
            Потратил - \(money)
            Транспорт - \(transport)
            Описание - \(details)
            """)
    }
}
