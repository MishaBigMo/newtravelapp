//
//  APIManger.swift
//  Travel App
//
//  Created by Михаил on 17/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Foundation
import Alamofire

class ApiManager {
    // pattern - singleTorn
    static var instance = ApiManager()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    
    private enum EndPoints {
        static let users = "/Users"
    }
    
    func getUsersDecodable( onComplete: @escaping ([User]?)-> Void ) {
        let urlString = Constants.baseURL + EndPoints.users
        let url = URL(string: urlString)!
        AF.request(url).responseData { (response) in
            if let data = response.value {
                let users = try? JSONDecoder().decode([User].self, from: data)
                onComplete(users)
            }
        }
    }
}
