//
//  Photo.swift
//  Travel App
//
//  Created by Михаил on 30/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class Photo {
    var albumId: Int = 0
    var id: Int = 0
    var title: String = ""
    var url: String = ""
    var thumbnailUrl: String = ""
}
