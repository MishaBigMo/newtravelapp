//
//  RealmTravels.swift
//  Travel App
//
//  Created by Михаил on 31/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation
import RealmSwift

class RealmTravel: Object {
    @objc dynamic var id: String = UUID().uuidString
    
    @objc dynamic var name: String = ""
    @objc dynamic var desc: String = ""
    var stops = List<RealmStop>()

    func getAverageRating() -> Int {
        if stops.count <= 0 {
            return 0
        }
        var summ: Int = 0
        for stop in stops {
            summ = summ + stop.rank
        }
        return summ/stops.count
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
